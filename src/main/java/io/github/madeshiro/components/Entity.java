package io.github.madeshiro.components;

import io.github.madeshiro.jdbc.SchoolDB;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.github.madeshiro.jdbc.SchoolDB.SqlCharSequence;

public abstract class Entity extends Component {

    protected String firstname, lastname;

    protected Entity(String firstname, String lastname) {
        super();

        this.firstname = firstname;
        this.lastname  = lastname;
    }

    protected Entity(ResultSet resultSet) throws SQLException {
        super(resultSet);

        this.firstname = resultSet.getString("FIRST_NAME");
        this.lastname  = resultSet.getString("LAST_NAME");
    }

    public String getFirstname() {
        return this.firstname;
    }

    public void setFirstname(String first) {
        this.firstname = first;
        set_sql_update("FIRST_NAME", SqlCharSequence(first));
    }

    public void setLastname(String last) {
        this.lastname = last;
        set_sql_update("LAST_NAME", SqlCharSequence(last));
    }

    public String getLastname() {
        return this.lastname;
    }

    @Override
    public String toString() {
        return String.format("{id: %d, first_name:'%s', last_name: '%s'}", getId(), getFirstname(), getLastname());
    }

    public List<Sticker> getStickers() {
        return SchoolDB.Instance().listFromAttribute(
                Sticker.class,
                SchoolDB.tableOf(this.getClass()),
                this.getId()
        );
    }

    /**
     * Get the full name of the entity (firstname + lastname)
     * @return the full name of the entity
     */
    public String getName() {
        return firstname + " " + lastname;
    }

    @Override
    public Map<String, Object> getFtlObject() {
        Map<String, Object> ftlObject = new HashMap<>();
        ftlObject.put("firstname", firstname);
        ftlObject.put("lastname", lastname);
        ftlObject.put("name", this.getName());

        ArrayList<Map<String, Object>> ftlStickersList = new ArrayList<>();
        for (Sticker sticker : getStickers()) {
            ftlStickersList.add(sticker.getFtlObject());
        }
        ftlObject.put("stickers", ftlStickersList);
        ftlObject.put("id", getId());

        return ftlObject;
    }

    @Override
    public Map<String, Object> getSimplifiedFtlObject() {
        Map<String, Object> ftlObject = new HashMap<>();
        ftlObject.put("firstname", firstname);
        ftlObject.put("lastname", lastname);
        ftlObject.put("name", this.getName());
        ftlObject.put("id", getId());

        return ftlObject;
    }
}
