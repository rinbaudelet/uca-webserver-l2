package io.github.madeshiro.components;

import io.github.madeshiro.jdbc.DataTable;
import io.github.madeshiro.jdbc.SchoolDB;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static io.github.madeshiro.jdbc.SchoolDB.SqlCharSequence;

@DataTable("STICKER")
public class Sticker extends Component {

    private StickerType type;
    private Date date;
    private String reason;
    private Student assignedStudent;
    private Professor author;

    public Sticker(StickerType type, Date date, Student student, Professor author) {
        super();

        this.type = type;
        this.date = date;
        this.assignedStudent = student;
        this.author = author;
    }

    public Sticker(StickerType type, Date date, String reason, Student student, Professor author) {
        this(type, date, student, author);

        this.reason = reason;
    }

    public Sticker(ResultSet set, Student student) throws SQLException {
        super(set);

        this.type = SchoolDB.Instance().fromId(StickerType.class, set.getInt("TYPE_ID"));
        this.date = set.getDate("ASSIGN");
        this.reason = set.getString("REASON");
        this.assignedStudent = student;
        this.author = SchoolDB.Instance().fromId(Professor.class, set.getInt("PROFESSOR"));
    }

    public Sticker(ResultSet set) throws SQLException {
        super(set);

        this.type = SchoolDB.Instance().fromId(StickerType.class, set.getInt("TYPE_ID"));
        this.date = set.getDate("ASSIGN");
        this.reason = set.getString("REASON");
        this.assignedStudent = SchoolDB.Instance().fromId(Student.class, set.getInt("STUDENT"));
        this.author = SchoolDB.Instance().fromId(Professor.class, set.getInt("PROFESSOR"));
    }

    public String getReason() {
        return this.reason;
    }

    public Date getDate() {
        return date;
    }

    public Professor getAuthor() {
        return author;
    }

    public StickerType getType() {
        return type;
    }

    public void setReason(String reason) {
        this.reason = reason;
        set_sql_update("REASON", SqlCharSequence(reason));
    }

    public void setStickerType(StickerType type) {
        this.type = type;
        set_sql_update("TYPE_ID", Integer.toString(type.getId()));
    }

    public void setDate(Date date) {
        this.date = date;
        set_sql_update("ASSIGN", SqlCharSequence(date.toString()));
    }

    @Override
    public String[] listSqlValues() {
        return new String[] {
                Integer.toString(type.getId()),
                SqlCharSequence(date.toString()),
                SqlCharSequence(reason.replace("'", "''")),
                Integer.toString(assignedStudent.getId()),
                Integer.toString(author.getId())
        };
    }

    public Student getStudent() {
        return assignedStudent;
    }

    @Override
    public String toJson() {
        return "{}"; // TODO
    }

    @Override
    public Map<String, Object> getFtlObject() {
        Map<String, Object> ftlObject = new HashMap<>();
        ftlObject.put("type", type.getFtlObject());
        ftlObject.put("id", getId());
        ftlObject.put("author", author.getSimplifiedFtlObject());
        ftlObject.put("student", assignedStudent.getSimplifiedFtlObject());
        ftlObject.put("date", date.toString());
        ftlObject.put("reason", reason);

        return ftlObject;
    }

    @Override
    public Map<String, Object> getSimplifiedFtlObject() {
        Map<String, Object> ftlObject = new HashMap<>();
        ftlObject.put("type", type.getFtlObject());
        ftlObject.put("id", getId());
        ftlObject.put("author", author.getName());
        ftlObject.put("student", assignedStudent.getName());
        ftlObject.put("date", date.toString());
        ftlObject.put("reason", reason);

        return ftlObject;
    }
}
