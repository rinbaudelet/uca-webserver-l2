package io.github.madeshiro.components;

import io.github.madeshiro.jdbc.DataTable;

import java.sql.ResultSet;
import java.sql.SQLException;

import static io.github.madeshiro.jdbc.SchoolDB.SqlCharSequence;

@DataTable("STUDENT")
public class Student extends Entity {

    public Student(String firstname, String lastname) {
        super(firstname, lastname);
    }

    public Student(ResultSet set) throws SQLException {
        super(set);
    }

    @Override
    public String[] listSqlValues() {
        return new String[] {
                SqlCharSequence(firstname),
                SqlCharSequence(lastname)
        };
    }

    @Override
    public String toJson() {
        return String.format("{" +
                "\"firstname\": \"%s\", \"lastname\": \"%s\"," +
                "\"id\": %d" +
                "}",
                firstname, lastname, getId()
        );
    }
}
