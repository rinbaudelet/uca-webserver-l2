package io.github.madeshiro.components;

import io.github.madeshiro.jdbc.DataTable;
import io.github.madeshiro.jdbc.SchoolDB;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.UUID;

import static io.github.madeshiro.jdbc.SchoolDB.SqlCharSequence;

@DataTable("PROFESSOR")
public class Professor extends Entity {
    private String passwd;
    private String username;
    private SessionDescriptor descriptor;

    public Professor(String firstname, String lastname, String username, String passwd) {
        super(firstname, lastname);

        this.username = username;
        this.passwd   = passwd;
        this.descriptor = null;
    }

    public Professor(ResultSet resultSet) throws SQLException {
        super(resultSet);

        this.username = resultSet.getString("USERNAME");
        this.passwd   = resultSet.getString("PASSWD");
        this.descriptor = null;
    }

    @Override
    public String[] listSqlValues() {
        return new String[]{
                SqlCharSequence(firstname),
                SqlCharSequence(lastname),
                SqlCharSequence(username),
                SqlCharSequence(passwd)
        };
    }

    public boolean isPassword(String passwd) {
        return this.passwd.equals(passwd);
    }

    @Override
    public String toJson() {
        return "{}"; // TODO
    }

    public SessionDescriptor createUserSession(String ip, String userAgent) {
        SessionDescriptor descriptor = SessionDescriptor.Generate(
                ip,
                userAgent,
                this
        );

        this.descriptor = SchoolDB.Instance().createComponent(
                SessionDescriptor.class, descriptor
        );
        return this.descriptor;
    }

    public void setDescriptor(SessionDescriptor descriptor) {
        this.descriptor = descriptor;
    }

    public String currentSID() {
        return descriptor.getUUID().toString();
    }

    public SessionDescriptor getDescriptor() {
        return descriptor;
    }

    public boolean revokeDescriptor() {
        if (descriptor != null) {
            return this.descriptor.revoke();
        }

        return false;
    }
}
