package io.github.madeshiro.components;

import io.github.madeshiro.jdbc.DataTable;
import io.github.madeshiro.jdbc.SchoolDB;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.github.madeshiro.jdbc.SchoolDB.SqlCharSequence;

@DataTable("STICKER_TYPE")
public class StickerType extends Component {

    private String color, description;

    public StickerType(String colorHex, String description) {
        this.color = colorHex;
        this.description = description;
    }

    public StickerType(ResultSet resultSet) throws SQLException {
        super(resultSet);

        this.color = resultSet.getString("COLOR");
        this.description = resultSet.getString("DESCRIPTION");
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        set_sql_update("DESCRIPTION", SqlCharSequence(description));
    }

    public String getColor() {
        return color;
    }

    public boolean setColor(String color) {
        if (CheckColorUnity(color, getId())) {
            this.color = color;
            set_sql_update("COLOR", SqlCharSequence(color));
        }

        return false;
    }

    @Override
    public String[] listSqlValues() {
        return new String[] {
                SqlCharSequence(color),
                SqlCharSequence(description)
        };
    }

    public static boolean CheckColorUnity(String hexcode) {
        for (StickerType type : SchoolDB.Instance().getStickerTypes()) {
            if (type.color.equals(hexcode))
                return false;
        }

        return true;
    }

    public static boolean CheckColorUnity(String hexcode, int excludeId) {
        for (StickerType type : SchoolDB.Instance().getStickerTypes()) {
            if (type.color.equals(hexcode) && type.getId() != excludeId)
                return false;
        }

        return true;
    }

    /**
     * Get all stickers which have for type this occurrence.
     * @return a list of Sticker with the same type as this
     */
    public List<Sticker> getStickers() {
        return SchoolDB.Instance().listFromAttribute(
                Sticker.class,
                "TYPE_ID",
                getId()
        );
    }



    @Override
    public String toJson() {
        return "{}"; // TODO
    }

    @Override
    public Map<String, Object> getFtlObject() {
        Map<String, Object> ftlObject = new HashMap<>();
        ftlObject.put("id", getId());
        ftlObject.put("color", color);
        ftlObject.put("description", description);


        return ftlObject;
    }

    @Override
    public Map<String, Object> getSimplifiedFtlObject() {
        Map<String, Object> ftlObject = new HashMap<>();
        ftlObject.put("id", getId());
        ftlObject.put("color", color);
        ftlObject.put("description", description);
        return ftlObject;
    }
}
