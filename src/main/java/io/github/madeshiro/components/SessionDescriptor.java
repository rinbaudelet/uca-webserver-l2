package io.github.madeshiro.components;

import io.github.madeshiro.jdbc.DataTable;
import io.github.madeshiro.jdbc.SchoolDB;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static io.github.madeshiro.jdbc.SchoolDB.SqlCharSequence;

@DataTable("SESSION")
public class SessionDescriptor extends Component
{
    private final UUID uuid;
    private final String ip_address;
    private final String user_agent;
    private final Professor professor;
    private final long timeout;

    public SessionDescriptor(UUID uuid, String ip, String user_agent, Professor prof) {
        super();

        this.uuid = uuid;
        this.ip_address = ip;
        this.user_agent = user_agent;
        this.professor  = prof;
        this.timeout = (System.currentTimeMillis()/1000L) + 3600*4;
    }

    public SessionDescriptor(ResultSet resultSet) throws SQLException {
        super(resultSet);

        this.uuid = UUID.fromString(resultSet.getString("SID"));
        this.ip_address = resultSet.getString("IP_ADDRESS");
        this.user_agent = resultSet.getString("USER_AGENT");
        this.professor  = SchoolDB.Instance().fromId(Professor.class, resultSet.getInt("USER_ID"));
        this.professor.setDescriptor(this);
        this.timeout = resultSet.getLong("TIMEOUT");
    }

    public static SessionDescriptor Generate(String ip, String user_agent, Professor user) {
        UUID uuid;
        do {
            uuid = UUID.randomUUID();
        }
        while (SchoolDB.Instance().fromAttribute(SessionDescriptor.class, "SID", uuid) != null);
        /**/
        return new SessionDescriptor(uuid, ip, user_agent, user);
    }

    @Override
    public String[] listSqlValues() {
        return new String[] {
                SqlCharSequence(ip_address),
                SqlCharSequence(user_agent),
                Integer.toString(professor.getId()),
                SqlCharSequence(uuid.toString()),
                String.valueOf(timeout)
        };
    }

    @Override
    public String toJson() {
        return "{}"; // TODO
    }

    @Override
    public Map<String, Object> getFtlObject() {
        Map<String, Object> ftlObject = new HashMap<>();
        ftlObject.put("uuid", uuid.toString());
        ftlObject.put("user", professor.getFtlObject());
        ftlObject.put("user_agent", user_agent);
        ftlObject.put("ip", ip_address);

        return ftlObject;
    }

    @Override
    public Map<String, Object> getSimplifiedFtlObject() {
        Map<String, Object> ftlObject = new HashMap<>();
        ftlObject.put("uuid", uuid.toString());
        ftlObject.put("user", professor.getSimplifiedFtlObject());
        ftlObject.put("user_agent", user_agent);
        ftlObject.put("ip", ip_address);

        return ftlObject;
    }

    public UUID getUUID() {
        return uuid;
    }

    public Professor getUser() {
        return professor;
    }

    public String getUserAgent() {
        return user_agent;
    }

    public String getIp() {
        return ip_address;
    }

    public boolean revoke() {
        if (getId() != -1) {
            return SchoolDB.Instance().deleteComponent(this);
        }

        return false;
    }

    public boolean isTimedOut() {
        return timeout < (System.currentTimeMillis()/1000L);
    }

    public long getTimeout() {
        return timeout;
    }
}
