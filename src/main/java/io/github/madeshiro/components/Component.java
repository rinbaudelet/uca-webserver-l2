package io.github.madeshiro.components;

import io.github.madeshiro.jdbc.SchoolDB;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static io.github.madeshiro.jdbc.SchoolDB.tableOf;

public abstract class Component {
    private final HashMap<String, String> _sql_update = new HashMap<>();

    protected final void set_sql_update(String label, String value) {
        if (id != -1)
            _sql_update.put(label, value);
    }

    /**
     * Get if some values have been put in the update's cache and then if the component
     * need to be updated in the Sql database.
     * @return True if some values has been changed, false otherwise.
     */
    public boolean needUpdate() {
        return !_sql_update.isEmpty();
    }

    /**
     * Update each value put in the component's cache into the Sql database.
     * @return True if the update has been done successfully, false otherwise.
     */
    public boolean updateSql() {
        if (id != -1 && tableOf(this.getClass()) != null) {
            try {
                StringBuilder SET = new StringBuilder();
                int row = 0;
                for (String label : _sql_update.keySet()) {
                    SET.append(String.format("%s = %s", label, _sql_update.get(label)));
                    if (row + 1 < _sql_update.size())
                        SET.append(" , ");

                    row++;
                }

                String query = String.format("UPDATE %s SET %s WHERE ID = %d;", tableOf(this.getClass()), SET, getId());
                PreparedStatement statement = SchoolDB.Instance().getConnection().prepareStatement(query);
                if (statement.executeUpdate() == 1) {
                    return true;
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        return false;
    }

    /**
     * The Unique Identifier for this component.
     * This field is private and immutable from external change in order to
     * guarantee that the id value reflect the true Java Object Representation of the
     * row from the database.
     *
     * @see io.github.madeshiro.jdbc.SchoolDB#createComponent
     */
    private final int id;

    public int getId() {
        return this.id;
    }

    public Component() {
        this.id = -1;
    }

    public Component(int id) {
        this.id = id;
    }

    public Component(ResultSet sqlResult) throws SQLException {
        this(sqlResult.getInt("id"));
    }

    /**
     * List in order (except ID) each Sql values to use to do an 'INSERT INTO'
     * @return a String list of each value of the component (except ID)
     */
    public abstract String[] listSqlValues();

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Component && obj.getClass().equals(getClass())) {
            return id == ((Component) obj).id;
        } else
            return false;
    }

    /**
     * Get a Json representation of the component.
     * @return a Json string
     */
    public abstract String toJson();

    /**
     * Get a Map representation of the component in order to use it for FreeMarker Template page generation.
     * @return a Map representation of the component.
     */
    public abstract Map<String, Object> getFtlObject();

    /**
     * Get a Map représentation of the component like {@link #getFtlObject()} but preventing
     * any kind of circle references (like between sticker and student).
     * @return a simplified map represention of the component
     */
    public abstract Map<String, Object> getSimplifiedFtlObject();
}
