package io.github.madeshiro.jdbc;

import java.lang.annotation.*;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface DataTable {
    /**
     * Get the table name associated with a component
     * @return
     */
    String value();
}
