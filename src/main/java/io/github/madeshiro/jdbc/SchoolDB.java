package io.github.madeshiro.jdbc;

import io.github.madeshiro.components.*;
import io.github.madeshiro.main.ServerMain;

import java.sql.*;
import java.util.HashMap;
import java.util.ArrayList;
import java.lang.reflect.InvocationTargetException;

public class SchoolDB {
    static final String db_url      = "jdbc:h2:./src/main/resources/db/webserver";
    static final String db_user     = "sa";
    static final String db_passwd   = "";

    private final Connection sqlConnection;
    private final HashMap<Class<? extends Component>, HashMap<Integer, Component>> _cache;
    
    public static SchoolDB Instance() {
        return ServerMain.Instance().getDB();
    }

    public SchoolDB() throws SQLException {
        sqlConnection = DriverManager.getConnection(db_url, db_user, db_passwd);
        sqlConnection.prepareStatement("use \"L2_WEBSERVER\";").execute();

        _cache = new HashMap<>();
        _cache.put(Sticker.class, new HashMap<>());
        _cache.put(StickerType.class, new HashMap<>());
        _cache.put(Student.class, new HashMap<>());
        _cache.put(Professor.class, new HashMap<>());
    }

    public Connection getConnection() {
        return sqlConnection;
    }

    public <T extends Component> void set_cache(T component) {
        _cache.get(component.getClass()).put(component.getId(), component);
    }

    public Component get_cache(Class<? extends Component> cClass, int id) {
        return _cache.get(cClass).get(id);
    }

    @SuppressWarnings("unchecked")
    public <T extends Component> T fromId(Class<T> componentClass, int id) {
        if (_cache.containsKey(componentClass) && _cache.get(componentClass).containsKey(id)) {
            return (T) get_cache(componentClass, id);
        }

        DataTable dataTable = componentClass.getAnnotation(DataTable.class);
        if (dataTable != null) {
            String query = String.format("SELECT * FROM %s WHERE id = %d;", dataTable.value(), id);
            try {
                PreparedStatement statement = this.sqlConnection.prepareStatement(query);
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    T component = componentClass.getConstructor(ResultSet.class).newInstance(resultSet);
                    set_cache(component);
                    return component;
                }
            } catch (SQLException | InvocationTargetException | InstantiationException | IllegalAccessException | NoSuchMethodException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public <T extends Component> T fromAttribute(Class<T> componentClass, String attrName, Object attr) {
        DataTable dataTable = componentClass.getAnnotation(DataTable.class);
        if (dataTable != null) {
            String query = String.format("SELECT * FROM %s WHERE %s = ?;", dataTable.value(), attrName);
            try {
                PreparedStatement statement = this.sqlConnection.prepareStatement(query);
                statement.setObject(1, attr);
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    return componentClass.getConstructor(ResultSet.class).newInstance(resultSet);
                }
            } catch (SQLException | InvocationTargetException | InstantiationException | IllegalAccessException | NoSuchMethodException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public <T extends Component> ArrayList<T> listFromAttribute(Class<T> componentClass, String attrName, Object attr) {
        DataTable dataTable = componentClass.getAnnotation(DataTable.class);
        ArrayList<T> list = new ArrayList<>();

        if (dataTable != null) {
            String query = String.format("SELECT * FROM %s WHERE %s = ?;", dataTable.value(), attrName);
            try {
                PreparedStatement statement = sqlConnection.prepareStatement(query);
                statement.setObject(1, attr);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    list.add(componentClass.getConstructor(ResultSet.class).newInstance(resultSet));
                }
            } catch (SQLException | InvocationTargetException | InstantiationException | IllegalAccessException | NoSuchMethodException e) {
                e.printStackTrace();
            }
        }

        return list;
    }

    public static String tableOf(Class<? extends Component> cls) {
        return cls.getAnnotation(DataTable.class).value();
    }

    public static String valuesOf(Component component) {
        StringBuilder values = new StringBuilder();
        values.append("default,");
        for (String value : component.listSqlValues()) {
            values.append(value).append(',');
        }

        values.deleteCharAt(values.length()-1);
        return values.toString();
    }

    public static String SqlCharSequence(String str) {
        return String.format("'%s'", str.replace("'", "''"));
    }

    public <T extends Component> ArrayList<T> getAllOf(Class<T> componentClass) {
        String table = tableOf(componentClass),
                query = String.format("SELECT * FROM %s;", table);
        try {
            ArrayList<T> components = new ArrayList<>();

            PreparedStatement statement = this.sqlConnection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                components.add(componentClass.getConstructor(ResultSet.class).newInstance(resultSet));
            }

            return components;
        } catch (SQLException | InvocationTargetException | InstantiationException | IllegalAccessException | NoSuchMethodException e) {
            e.printStackTrace();
        }

        return null;
    }

    public ArrayList<Student> getAllStudents() {
        return getAllOf(Student.class);
    }

    public ArrayList<Professor> getAllProfessors() {
        return getAllOf(Professor.class);
    }

    public ArrayList<Sticker> getAllStickers() {
        return getAllOf(Sticker.class);
    }

    public ArrayList<StickerType> getStickerTypes() {
        return getAllOf(StickerType.class);
    }

    public <T extends Component> T createComponent(Class<T> tClass, T from) {
        String query = String.format("INSERT INTO %s VALUES (%s);", tableOf(tClass), valuesOf(from));

        try {
            PreparedStatement statement = this.sqlConnection.prepareStatement(query);
            if (statement.executeUpdate() > 0) {
                statement = this.sqlConnection.prepareStatement("SELECT * FROM " + tableOf(tClass) + ";");
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.last()) {
                    return tClass.getConstructor(ResultSet.class).newInstance(resultSet);
                }
            }
        } catch (SQLException | InvocationTargetException | InstantiationException | IllegalAccessException | NoSuchMethodException e) {
            e.printStackTrace();
        }

        return null;
    }

    public boolean deleteComponent(Component component) {
        String table = tableOf(component.getClass());
        if (table != null) {
            try {
                return sqlConnection.prepareStatement("DELETE FROM " + table + " WHERE id = " + component.getId() + ";")
                        .executeUpdate() > 0;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    public void clearCache() {
        for (Class<?> keyClass : _cache.keySet()) {
            _cache.get(keyClass).clear();
        }
    }
}
