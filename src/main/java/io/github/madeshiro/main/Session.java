package io.github.madeshiro.main;

import freemarker.template.Template;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import io.github.madeshiro.components.Professor;
import io.github.madeshiro.components.SessionDescriptor;
import io.github.madeshiro.components.StickerType;
import io.github.madeshiro.jdbc.SchoolDB;
import spark.Request;
import spark.Response;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

public class Session {
    static Session _Session = null;

    private final Map<String, Object> _ftlRoot;
    private final spark.Session sparkSession;
    private final Request request;
    private final Response response;

    private Professor admin;

    /**
     * Create a new Session representation of a connection to the application
     * @param sparkSession the session object of Spark's framework
     * @param request the request object of Spark's framework
     * @param response the response object of Spark's framework
     */
    Session(spark.Session sparkSession, Request request, Response response) {
        this.sparkSession = sparkSession;
        this.request = request;
        this.response = response;

        _ftlRoot = new HashMap<>();

        String user_sid = sparkSession.attribute("user_sid");
        if (user_sid == null && request.cookies().containsKey("session_id"))
            user_sid = request.cookie("session_id");

        if (user_sid != null) {
            if (login(UUID.fromString(user_sid))) {
                HashMap<String, Object> user = new HashMap<>();
                user.put("name", admin.getName());
                user.put("firstname", admin.getFirstname());
                user.put("lastname", admin.getLastname());

                _ftlRoot.put("user", user);
            }
        }

        _ftlRoot.put("is_login", isLogin());
    }

    public spark.Session getSparkSession() {
        return sparkSession;
    }

    /**
     * Gets if the user is logged in as an administrator (professor)
     * @return if the user is logged in as an administrator (professor)
     */
    public boolean isLogin() {
        return admin != null;
    }

    /**
     * Authenticate the user. If the Authentication is successful, the SID will be registered into
     * the session and the cache.
     * @return
     * <ul>
     *     <li>0 - Login successful</li>
     *     <li>1 - User Not Found</li>
     *     <li>2 - Invalid password</li>
     *     <li>3 - Already connected</li>
     * </ul>
     */
    public int login(String username, String passwd) {
        if (admin == null) {
            Professor user = SchoolDB.Instance().fromAttribute (
                    Professor.class, "USERNAME", username
            );
            if (user != null) {
                if (user.isPassword(passwd)) {
                    user.createUserSession(request.ip(), request.userAgent());

                    this.admin = user;
                    sparkSession.attribute("user_sid", user.currentSID());
                    response.cookie("session_id",
                            user.currentSID(),
                            3600 * 4,
                            true
                    );
                    return 0;
                } else {
                    return 2;
                }
            } else {
                return 1;
            }
        } else
            return 3;
    }

    /**
     *
     * @param sid the Universally Unique Identifier of the user session to log in.
     * @return True if the SID is valid and enable the user to log in, false otherwise.
     */
    private boolean login(UUID sid) {
        SessionDescriptor descriptor = SchoolDB.Instance().fromAttribute(
                SessionDescriptor.class, "SID", sid
        );

        if (descriptor != null) {
            if (!descriptor.isTimedOut()) {
                admin = descriptor.getUser();
            } else {
                descriptor.revoke();
            }
        }

        if (admin == null) {
            sparkSession.removeAttribute("user_sid");
            response.removeCookie("session_id");
            return false;
        }

        return true;
    }

    /**
     * Log out the user from its current device.
     */
    public void logout() {
        admin.revokeDescriptor();

        sparkSession.removeAttribute("user_sid");
        response.removeCookie("session_id");
        admin = null;
    }

    /**
     * Log out the user from every device where the user were logged on.
     */
    public void logoutFromEverywhere() {
        try {
            PreparedStatement statement = ServerMain.Instance().getSqlConnection().prepareStatement(
                    "DELETE FROM SESSION WHERE USER_ID = " + admin.getId() + ";"
            );
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        sparkSession.removeAttribute("user_sid");
        response.removeCookie("session_id");
        admin = null;
    }

    /**
     * Get the Map root for the FreeMarker template generator.
     * @return the map root fot FTL generation
     */
    public Map<String, Object> ftlRoot() {
        return _ftlRoot;
    }

    /**
     * Generate a http web page to display error the user. HTTP Status will be set up to as the
     * value passed in
     * @param code the http status code
     * @param title the http status title name
     * @param desc the error description
     * @return an HTML page generated by FreeMarker.
     */
    public String httpError(int code, String title, String desc) {
        _ftlRoot.put("error_code", code);
        _ftlRoot.put("error_title", title);
        _ftlRoot.put("error_description", desc);
        _ftlRoot.put("body", "part/errorPage.ftl");

        response.status(code);
        return loadPage("model.ftl", String.format("Error page - %d", code));
    }

    public String httpResponse(int code, String title, String desc, String redirection) {
        if (redirection == null) {
            redirection = "/";
        }

        _ftlRoot.put("done_title", title);
        _ftlRoot.put("done_description", desc);
        _ftlRoot.put("body", "part/donePage.ftl");
        _ftlRoot.put("redirection_link", redirection);

        response.status(code);
        return loadPage("model.ftl", String.format("Request done - %d", code));
    }

    public String loadPage(String template, String title) {
        Configuration cfg = ServerMain.Instance().getCfg();
        response.type("text/html");

        List<Map<String, Object>> stickerTypes = new ArrayList<>();
        for (StickerType type : SchoolDB.Instance().getStickerTypes()) {
            stickerTypes.add(type.getFtlObject());
        }
        _ftlRoot.put("sticker_types", stickerTypes);
        _ftlRoot.put("title", title);

        if (isLogin()) {
            _ftlRoot.put("user", userLogin().getFtlObject());
        }

        try {
            Template page = cfg.getTemplate(template);
            Writer out = new StringWriter();
            page.process(_ftlRoot, out);

            return out.toString();
        } catch (IOException | TemplateException e) {
            throw new RuntimeException(e);
        }
    }

    public Professor userLogin() {
        return this.admin;
    }
}
