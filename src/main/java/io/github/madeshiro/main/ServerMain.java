package io.github.madeshiro.main;

import freemarker.template.Configuration;
import freemarker.template.TemplateExceptionHandler;

import io.github.madeshiro.jdbc.SchoolDB;
import io.github.madeshiro.request.IRequest;
import io.github.madeshiro.request.Method;
import io.github.madeshiro.request.RequestListener;
import io.github.madeshiro.request.annotation.RequestRoutes;
import io.github.madeshiro.request.filters.Afterable;
import io.github.madeshiro.request.filters.Beforeable;

import spark.Route;
import spark.RouteGroup;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;

import static spark.Spark.*;

/**
 *
 * <h3>List of all URIs:</h3>
 * <ul>
 *     <li><a color="green">GET</a> <u color="blue">/</u> Index page</li>
 *     <li><a color="green">GET</a> <u color="blue">/login</u> Login page</li>
 *     <li><a color="green">GET</a> <u color="blue">/logout</u> Logout</li>
 *     <li><u color="blue">/stickers</u></li>
 *     <ul>
 *         <li><a color="green">GET</a> Display a list of each sticker's type</li>
 *         <li><u color="blue">/:hex</u></li>
 *         <ul>
 *             <li><a color="green">GET</a> Display a sticker type</li>
 *             <li><a color="green">POST</a> <a color="gray">?desc=</a> Create a new sticker type</li>
 *             <li><a color="green">PUT</a> <a color="gray">?color=&desc=</a> Modify a sticker</li>
 *             <li><a color="green">DELETE</a> Delete a sticker type</li>
 *         </ul>
 *     </ul>
 *     <li><u color="blue">/students</u></li>
 *     <ul>
 *         <li><a color="green">GET</a> Display a list of each students</li>
 *         <li><a color="green">POST</a> <a color="gray">?first=&last=</a> Create a new student</li>
 *     </ul>
 *     <ul>
 *         <li><u color="blue">/:name</u></li>
 *         <ul>
 *             <li><a color="green">GET</a> Display the information about a student and all its stickers</li>
 *             <li><a color="green">DELETE</a> Delete the student</li>
 *             <li>
 *                 <a color="green">PUT</a> <a color="gray">?first=&last=</a> Modify a student.
 *                  Modifiable value can be 'first', 'last' or both
 *             </li>
 *         </ul>
 *     </ul>
 *     <li><u color="blue">/students/:name/stickers</u></li>
 *     <ul>
 *         <li><a color="green">GET</a> Display a list of stickers assigned to the student</li>
 *         <li><u color=blue>/:sticker_id</u></li>
 *         <ul>
 *             <li><a color="green">GET</a> Get information about the sticker</li>
 *             <li><a color="green">PUT</a> <a color="gray">?date=&reason=&professor=&type=</a> Modify the sticker</li>
 *             <li><a color="green">DELETE</a> Delete the sticker</li>
 *         </ul>
 *     </ul>
 * </ul>
 */
public final class ServerMain {
    private static ServerMain _Instance;

    private final Configuration cfg;
    private final SchoolDB db;

    /**
     * Gets the current instance of the server
     * @return the server object's instance
     */
    public static ServerMain Instance() {
        return _Instance;
    }

    public static Session Session() {
        return Session._Session;
    }

    public static void main(String[] args) {
        try {
            _Instance = new ServerMain();
            _Instance.initSpark();
        } catch (IOException | SQLException e) {
            e.printStackTrace();
            stop();
        }
    }

    public ServerMain() throws IOException, SQLException {
        staticFiles.location("/static/");
        port(8081);

        this.cfg = new Configuration(Configuration.VERSION_2_3_31);
        cfg.setClassForTemplateLoading(ServerMain.class, "/htmlPages");
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        cfg.setLogTemplateExceptions(false);
        cfg.setWrapUncheckedExceptions(true);
        cfg.setFallbackOnNullLoopVariable(false);

        this.db = new SchoolDB();
    }

    public static void addRoute(Method method, String uri, Route route) {
        switch (method) {
            case GET:
                get(uri, route);
                break;
            case POST:
                post(uri, route);
                break;
            case PUT:
                put(uri, route);
                break;
            case DELETE:
                delete(uri, route);
                break;
            case PATCH:
                patch(uri, route);
                break;
            case OPTIONS:
                options(uri, route);
                break;
            default:
                System.err.println("Invalid HTTP/Verb required !");
                break;
        }
    }

    public void initSpark() {
        afterAfter("/*", (request, response) -> SchoolDB.Instance().clearCache());

        before("/*", (request, response) -> {
            request.session(true);
            System.out.println(request.requestMethod() +" " +request.uri());
            Session._Session = new Session(request.session(), request, response);
        });

        notFound((request, response) -> Session().httpError(404,
                "Not Found",
                "The page you're asking for doesn't exist !"
                )
        );

        for (Class<? extends IRequest> cls : Method.getMethods()) {
            for (IRequest request : cls.getEnumConstants()) {
                try {
                    Route instance = request.instantiate();
                    addRoute(request.method(), request.uri(), instance);
                    System.out.printf("instantiate route %s @ %s\n", request.method(), request.uri());

                    if (instance instanceof RouteGroup) {
                        path(request.uri(), (RouteGroup) instance);
                        System.out.printf("instantiate route Spark.path\t\t@ %s\n", request.uri());
                    }
                    if (instance instanceof Beforeable) {
                        before(request.uri(), ((Beforeable) instance).before());
                        System.out.printf("instantiate route Spark.before\t@ %s\n", request.uri());
                    }
                    if (instance instanceof Afterable) {
                        after(request.uri(), ((Afterable) instance).after());
                        System.out.printf("instantiate route Spark.after\t@ %s\n", request.uri());
                    }
                }  catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        for (Method.Listeners listeners : Method.Listeners.values()) {
            try {
                RequestListener instance = listeners.instantiate();
                RequestRoutes annot = instance.getClass().getAnnotation(RequestRoutes.class);

                if (annot != null) {
                    System.out.printf("+ start instantiation of %s @ %s %s\n", instance.getClass().getSimpleName(), annot.uri(), Arrays.toString(annot.methods()));
                    for (Method method : annot.methods()) {
                        if (instance.route(method) != null) {
                            addRoute(method, annot.uri(), instance.route(method));
                            System.out.printf(">\tinstantiate listener %s\t@ %s\n", method.name(), annot.uri());
                        } else {
                            System.err.printf("method %s expected but not implemented in listener %s@%s", method.name(),
                                    instance.getClass().getSimpleName(), annot.uri());
                        }
                    }

                    if (instance instanceof RouteGroup) {
                        path(annot.uri(), (RouteGroup) instance);
                        System.out.printf(">\tinstantiate listener Spark.path\t\t@ %s\n", annot.uri());
                    }
                    if (instance instanceof Beforeable) {
                        before(annot.uri(), ((Beforeable) instance).before());
                        System.out.printf(">\tinstantiate listener Spark.before\t@ %s\n", annot.uri());
                    }
                    if (instance instanceof Afterable) {
                        after(annot.uri(), ((Afterable) instance).after());
                        System.out.printf(">\tinstantiate listener Spark.after\t@ %s\n", annot.uri());
                    }
                }
            } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public Configuration getCfg() {
        return this.cfg;
    }

    public SchoolDB getDB() {
        return this.db;
    }

    public Connection getSqlConnection() {
        return this.db.getConnection();
    }
}
