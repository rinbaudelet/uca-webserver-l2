package io.github.madeshiro.request.annotation;

import io.github.madeshiro.request.Method;

import java.lang.annotation.*;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface RequestRoute {
    Method method();
    String uri();
}
