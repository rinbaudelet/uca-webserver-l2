package io.github.madeshiro.request.filters;

import spark.Filter;
import spark.Request;
import spark.Response;

public interface Afterable {
    /**
     * The filter to execute after the route function
     * @return the filter to plug into Spark#after
     * @see spark.Spark#after(String, Filter)
     */
    Filter after();
}
