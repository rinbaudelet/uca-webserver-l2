package io.github.madeshiro.request.filters;

import spark.Filter;

public interface Beforeable {
    /**
     * The filter to execute before the route function
     * @return the filter to plug into Spark#before
     * @see spark.Spark#before(String, Filter)
     */
    Filter before();
}
