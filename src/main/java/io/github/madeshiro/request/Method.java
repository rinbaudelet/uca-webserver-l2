package io.github.madeshiro.request;

import io.github.madeshiro.request.listeners.StickerTypeListener;
import io.github.madeshiro.request.listeners.StickersOfStudentListener;
import io.github.madeshiro.request.listeners.StudentListener;

import spark.Route;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

public enum Method {
    GET,
    POST,
    PUT,
    PATCH,
    OPTIONS,
    DELETE;

    public static List<Class<? extends IRequest>> getMethods() {
        return Arrays.asList(Get.class, Post.class);
    }

    public enum Get implements IRequest {
        Index(io.github.madeshiro.request.listeners.get.Index.class),
        Login(io.github.madeshiro.request.listeners.get.Login.class),
        Logout(io.github.madeshiro.request.listeners.get.Logout.class),
        Students(io.github.madeshiro.request.listeners.get.Students.class),
        Stickers(io.github.madeshiro.request.listeners.get.Stickers.class);

        private final Class<? extends Route> cls;
        Get(Class<? extends Route> cls) {
            this.cls = cls;
        }

        @Override
        public Class<? extends Route> requestClass() {
            return this.cls;
        }
    }

    public enum Post implements IRequest {
        Login(io.github.madeshiro.request.listeners.post.Login.class),
        Students(io.github.madeshiro.request.listeners.post.Students.class);

        private final Class<? extends Route> cls;
        Post(Class<? extends Route> cls) {
            this.cls = cls;
        }

        @Override
        public Class<? extends Route> requestClass() {
            return this.cls;
        }
    }

    public enum Listeners {
        StudentLSTN(StudentListener.class),
        StickerLSTN(StickerTypeListener.class),
        StickerOfStudentLSTN(StickersOfStudentListener.class);

        private final Class<? extends RequestListener> cls;
        Listeners(Class<? extends RequestListener> cls) {
            this.cls = cls;
        }

        public RequestListener instantiate() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
            return cls.getConstructor().newInstance();
        }

        public Class<? extends RequestListener> getListenerClass() {
            return this.cls;
        }
    }
}
