package io.github.madeshiro.request;

import io.github.madeshiro.request.annotation.RequestRoute;
import spark.Route;

import java.lang.reflect.InvocationTargetException;

public interface IRequest {
    String name();
    default Method method() {
        return this.requestClass().getAnnotation(RequestRoute.class).method();
    }

    default String uri() {
        return this.requestClass().getAnnotation(RequestRoute.class).uri();
    }

    default Route instantiate() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        return this.requestClass().getConstructor().newInstance();
    }
    Class<? extends Route> requestClass();
}
