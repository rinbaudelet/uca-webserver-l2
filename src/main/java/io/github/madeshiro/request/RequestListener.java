package io.github.madeshiro.request;

import spark.Route;
import spark.Spark;

import java.util.EnumMap;
import java.util.Set;

public abstract class RequestListener {
    private final EnumMap<Method, Route> routes = new EnumMap<>(Method.class);

    /**
     * Defines the routes of the listener. This function won't register directly them
     * into Spark. The registration happens only at the server start and if this function
     * has the {@link io.github.madeshiro.request.annotation.RequestRoutes annotation}.
     * @param method
     * @param route
     */
    protected void setRoute(Method method, Route route) {
        routes.put(method, route);
    }

    /**
     * Get the route associated to the associated HTTP's verb
     * @param method the HTTP's verb to get the route from
     * @return The route associated to the requested method if exists, otherwise null;
     */
    public Route route(Method method) {
        return routes.get(method);
    }

    /**
     * List the current methods supported by this listener.
     * @implNote it returns the method implemented in the listener,
     * registered in the EnumMap and not eventually any kind of annotation
     * @return a Set of HTTP's method supported by this listener.
     */
    public Set<Method> listMethods() {
        return routes.keySet();
    }
}
