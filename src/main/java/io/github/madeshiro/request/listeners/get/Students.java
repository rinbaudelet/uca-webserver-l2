package io.github.madeshiro.request.listeners.get;

import io.github.madeshiro.components.Student;
import io.github.madeshiro.jdbc.SchoolDB;
import io.github.madeshiro.request.annotation.RequestRoute;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static io.github.madeshiro.main.ServerMain.Session;
import static io.github.madeshiro.request.Method.GET;

@RequestRoute(method = GET, uri = "/students")
public class Students implements Route {
    @Override
    public Object handle(Request request, Response response) {
        Map<String, Object> ftlRoot = Session().ftlRoot();
        ftlRoot.put("body", "part/listStudent.ftl");

        List<Map<String, Object>> students = new ArrayList<>();
        for (Student student : SchoolDB.Instance().getAllStudents()) {
            students.add(student.getFtlObject());
        }
        ftlRoot.put("students", students);

        return Session().loadPage("model.ftl", "List of students");
    }
}
