package io.github.madeshiro.request.listeners.get;

import io.github.madeshiro.request.annotation.RequestRoute;
import spark.Request;
import spark.Response;
import spark.Route;

import static io.github.madeshiro.main.ServerMain.Session;
import static io.github.madeshiro.request.Method.GET;

@RequestRoute(method = GET, uri = "/login")
public class Login implements Route {
    @Override
    public Object handle(Request request, Response response) {
        Session().ftlRoot().put("title", "Login");
        Session().ftlRoot().put("body", "part/loginPage.ftl");
        return Session().loadPage("model.ftl", "Login page");
    }
}
