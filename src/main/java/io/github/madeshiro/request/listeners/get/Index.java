package io.github.madeshiro.request.listeners.get;

import freemarker.template.Template;
import io.github.madeshiro.main.ServerMain;
import io.github.madeshiro.request.annotation.RequestRoute;
import spark.Request;
import spark.Response;
import spark.Route;

import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import static io.github.madeshiro.main.ServerMain.Session;
import static io.github.madeshiro.request.Method.GET;

@RequestRoute(method = GET, uri = "/")
public class Index implements Route {
    @Override
    public Object handle(Request request, Response response) throws Exception {
        response.type("text/html");
        Session().ftlRoot().put("title", "Home");
        Session().ftlRoot().put("body", "part/index.ftl");

        return Session().loadPage("model.ftl", "Home");
    }
}
