package io.github.madeshiro.request.listeners.post;

import io.github.madeshiro.request.annotation.RequestRoute;
import spark.Request;
import spark.Response;
import spark.Route;

import static io.github.madeshiro.main.ServerMain.Session;
import static io.github.madeshiro.request.Method.POST;

@RequestRoute(method = POST, uri = "/login")
public class Login implements Route {
    @Override
    public Object handle(Request request, Response response) {
        String username = request.queryParams("user"),
                passwd = request.queryParams("pwd");

        String errorMsg = "";
        int status = 4; // 4 equals MISSING_ARGUMENT
        if (username != null && passwd != null) {
            status = Session().login(username, passwd);
            switch (status) {
                case 0:
                    response.type("text/html");
                    response.status(200);
                    return "Successfully connected as " + Session().userLogin().getName();
                case 1:
                    errorMsg = "Invalid username !";
                    break;
                case 2:
                    errorMsg = "Invalid password !";
                    break;
                case 3:
                    response.type("text/html");
                    response.status(202);
                    return "Already connected as " + Session().userLogin().getName() + "\n";
            }
        }

        response.status(400);
        response.type("application/json");
        return String.format("{\"message\":\"%s\", \"error_id\": %d}",
                errorMsg, status);
    }
}
