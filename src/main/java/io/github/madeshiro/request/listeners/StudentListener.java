package io.github.madeshiro.request.listeners;

import io.github.madeshiro.components.Sticker;
import io.github.madeshiro.components.StickerType;
import io.github.madeshiro.jdbc.SchoolDB;
import io.github.madeshiro.components.Student;
import io.github.madeshiro.request.RequestListener;
import io.github.madeshiro.request.filters.Beforeable;
import io.github.madeshiro.request.annotation.RequestRoutes;

import spark.Route;
import spark.Spark;
import spark.Filter;
import spark.RouteGroup;

import java.sql.*;
import java.util.Collections;

import static io.github.madeshiro.main.ServerMain.Session;
import static io.github.madeshiro.request.Method.*;
import static spark.Spark.*;

@RequestRoutes(methods = {GET, DELETE, PUT}, uri = "/students/:name")
public class StudentListener extends RequestListener implements RouteGroup, Beforeable {
    private int id;
    private Student student;

    public final Route get = (request, response) -> {
        Session().ftlRoot().put("student", student.getFtlObject());
        return Session().loadPage("part/displayStudent.ftl", "Student - " + student.getName());
    };

    public final Route delete = (request, response) -> {
        if (Session().isLogin()) {
            SchoolDB.Instance().deleteComponent(student);
            response.status(200);
            response.type("text/plain");
            return String.format("Successfully delete student (id = %d).", id);
        } else {
            return Session().httpError(401,
                    "Unauthorized",
                    "You need to be log in to remove a student");
        }
    };

    /**
     * Update student's information
     * query params are 'first' and 'last'. No need to put both at the same time.
     */
    public final Route put = (request, response) -> {
        if (Session().isLogin()) {
            String first = request.queryParams("first");
            if (first != null) {
                student.setFirstname(first);
            }

            String last = request.queryParams("last");
            if (last != null) {
                student.setLastname(last);
            }

            if (student.needUpdate()) {
                if (student.updateSql()) {
                    return Session().httpResponse(202, "Student modified !",
                            "Student's information has been updated successfully !",
                            String.format("/student/%d", student.getId())
                    );
                } else {
                    return Session().httpError(500, "Internal Server Error",
                            "An error occurred while updating student information..."
                    );
                }
            } else {
                return Session().httpError(400, "Invalid Request",
                        "You need to put at least one param to the request (first and/or last)");
            }
        } else {
            return Session().httpError(401, "Unauthorized",
                    "You need to be logged in to modify a student"
            );
        }
    };

    private final Filter _before = (request, response) -> {
        try {
            this.id = Integer.parseInt(request.params(":name"));
            student = SchoolDB.Instance().fromId(Student.class, id);
            if (student == null) {
                halt(404,
                        Session().httpError(404,
                                "Not Found",
                                String.format("Student (id = %d) not found !", id)
                        )
                );
            } else {
                Session().ftlRoot().put("students", Collections.singletonList(student.getFtlObject()));
            }
        } catch (NumberFormatException e) {
            halt(400,
                    Session().httpError(400,
                            "Bad Request",
                            "Wrong student's ID format (expected number)")
            );
        }
    };

    /**
     * List all stickers for one student
     */
    public final Route getStickers =  (request, response) -> {
        if (request.contentType().equals("application/json")) {
            StringBuilder json = new StringBuilder("{");
            Connection connection = SchoolDB.Instance().getConnection();
            try {
                PreparedStatement statement = connection.prepareStatement(
                        "SELECT * FROM STICKER WHERE STUDENT = ?;"
                );
                statement.setInt(1, id);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    Sticker sticker = new Sticker(resultSet);
                    json.append(
                            sticker.toJson()
                    ).append(',');
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }

            json.setCharAt(json.length()-1, '}');
            return json.toString();
        } else {
            return get.handle(request, response);
        }
    };

    /**
     * Create a new sticker for a student
     * query params are 'color', 'reason', 'date'
     */
    public final Route postStickers = (request, response) -> {
        if (Session().isLogin()) {
            String queryColor = request.queryParams("color");
            String queryReason = request.queryParams("reason");
            String queryDate = request.queryParams("date");

            if (queryColor != null && queryReason != null && queryDate != null) {
                Date date = Date.valueOf(queryDate);
                StickerType type = SchoolDB.Instance().fromAttribute(StickerType.class, "COLOR", queryColor);
                if (date != null && type != null) {
                    if (SchoolDB.Instance().createComponent(Sticker.class,
                            new Sticker(
                                    type, date, queryReason, student, Session().userLogin()
                            )
                    ) != null) {
                        return Session().httpResponse(201, "Sticker created !",
                                "The sticker has been created successfully !",
                                String.format("/student/%d/stickers", student.getId())
                        );
                    }
                }
            }

            return Session().httpError(400,
                    "Bad Request",
                    "Missing argument to create a new sticker or invalid argument (date, color, reason) !"
            );
        } else {
            return Session().httpError(401,
                    "Unauthorized",
                    "You're not authorized to do that");
        }
    };

    public StudentListener() {
        setRoute(GET, get);
        setRoute(DELETE, delete);
        setRoute(PUT, put);
    }

    @Override
    public void addRoutes() {
        Spark.before("/*", this._before);

        get("/stickers", this.getStickers);
        post("/stickers", this.postStickers);
    }

    @Override
    public Filter before() {
        return this._before;
    }
}
