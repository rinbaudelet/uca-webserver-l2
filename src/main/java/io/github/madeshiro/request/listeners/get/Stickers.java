package io.github.madeshiro.request.listeners.get;

import io.github.madeshiro.components.StickerType;
import io.github.madeshiro.jdbc.SchoolDB;
import io.github.madeshiro.main.ServerMain;
import io.github.madeshiro.main.Session;
import io.github.madeshiro.request.annotation.RequestRoute;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;
import java.util.Map;

import static io.github.madeshiro.request.Method.GET;

@RequestRoute(method = GET, uri = "/stickers")
public class Stickers implements Route {
    @Override
    public Object handle(Request request, Response response) {
        Session session = ServerMain.Session();
        session.ftlRoot().put("body", "part/listStickerTypes.ftl");

        ArrayList<StickerType> stickerTypes = SchoolDB.Instance().getStickerTypes();
        ArrayList<Map<String, Object>> ftlStickers = new ArrayList<>();
        for (StickerType type : stickerTypes) {
            ftlStickers.add(type.getFtlObject());
        }
        session.ftlRoot().put("stickers", ftlStickers);

        return session.loadPage("model.ftl", "Sticker types list");
    }
}
