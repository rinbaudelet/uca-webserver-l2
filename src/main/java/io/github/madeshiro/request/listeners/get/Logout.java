package io.github.madeshiro.request.listeners.get;

import io.github.madeshiro.main.ServerMain;
import io.github.madeshiro.main.Session;
import io.github.madeshiro.request.annotation.RequestRoute;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.Objects;

import static io.github.madeshiro.request.Method.GET;

@RequestRoute(method = GET, uri = "/logout")
public class Logout implements Route {
    @Override
    public Object handle(Request request, Response response) {
        Session servSession = ServerMain.Session();

        if (ServerMain.Session().isLogin()) {
            if (Objects.equals(request.queryParams("fromEverywhere"), "true")) {
                servSession.logoutFromEverywhere();
            } else {
                servSession.logout();
            }
            return "logout !";
        } else {
            return servSession.httpError(401,
                    "Unauthorized",
                    "Unable to logout if you're not even login !"
            );
        }
    }
}
