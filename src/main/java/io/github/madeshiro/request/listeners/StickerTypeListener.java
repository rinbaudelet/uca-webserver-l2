package io.github.madeshiro.request.listeners;

import io.github.madeshiro.components.StickerType;
import io.github.madeshiro.jdbc.SchoolDB;
import io.github.madeshiro.request.RequestListener;
import io.github.madeshiro.request.annotation.RequestRoutes;
import io.github.madeshiro.request.filters.Beforeable;
import spark.Filter;
import spark.Route;

import static io.github.madeshiro.main.ServerMain.Session;
import static io.github.madeshiro.request.Method.*;
import static spark.Spark.halt;

/**
 * A listener to execute every action concerning sticker's types (except showing them,
 * see {@link io.github.madeshiro.request.listeners.get.Stickers Stickers} to get more info about this request).
 *
 * @implNote the user need to be log in to call one of this method !
 */
@RequestRoutes(methods = {POST, PUT, DELETE}, uri = "/stickers/:hex")
public class StickerTypeListener extends RequestListener implements Beforeable {
    /**
     * the color code currently requested.
     */
    private String hexcode;
    /**
     * The sticker's type currently requested.
     */
    private StickerType stickerType;

    /**
     * Create a new sticker type, including a description using query param ?desc=...
     */
    public final Route post   = (request, response) -> {
        String description = request.queryParams("desc");
        if (description == null) {
            return Session().httpError(400, "Invalid Request",
                    "You need to put a description for the sticker's type"
            );
        }

        // if the sticker type already exists with this color, POST cannot continue
        if (stickerType == null) {
            stickerType = SchoolDB.Instance().createComponent(StickerType.class, new StickerType(hexcode, description));
            if (stickerType != null) {
                return Session().httpResponse(200,
                        "Create sticker's type #" + hexcode,
                        "The sticker type has been created successfully !",
                        "/stickers"
                );
            } else {
                return Session().httpError(500,
                        "Internal Error Server",
                        "An error occurred server's side while creating the sticker's type"
                );
            }
        } else {
            return Session().httpError(409, "Conflict",
                    "Sticker with color #" + hexcode + " already exists !"
            );
        }
    };

    /**
     * Update a sticker type.
     */
    public final Route put = (request, response) -> {
        String hex = request.queryParams("color");
        if (hex != null) {
            if (hex.matches("[0-9a-fA-F]{6}") && hex.length() == 6) {
                stickerType.setColor(hex);
            } else {
                return Session().httpError(400,
                        "Invalid Request",
                        "Invalid hexadecimal color format."
                );
            }
        }

        String description = request.queryParams("desc");
        if (description != null) {
            stickerType.setDescription(description);
        }

        if (stickerType.needUpdate()) {
            if (stickerType.updateSql()) {
                return Session().httpResponse(200, "Update sticker's type",
                        "The sticker's type has been updated successfully !",
                        "/stickers"
                );
            } else {
                return Session().httpError(500, "Internal Server Error",
                        "An error occurred server's side while updating the sticker's type"
                );
            }
        } else {
            return Session().httpError(400, "Bad Request",
                    "Missing request's body !");
        }
    };

    /**
     * Delete a sticker type.
     * @implNote By deleting a sticker type, every sticker associated with will be deleted too.
     */
    public final Route delete = (request, response) -> {
        if (SchoolDB.Instance().deleteComponent(stickerType)) {
            return Session().httpResponse(200,
                    "Delete sticker's type #" + hexcode,
                    "The sticker type #" + hexcode + " has been deleted successfully !",
                    "/stickers"
            );
        }

        return Session().httpError(500,
                "Internal Server Error",
                "An error occurred server's side while deleting the sticker's type"
        );
    };

    private final Filter _before = (request, response) -> {
        if (Session().isLogin()) {
            this.hexcode = request.params(":hex");
            this.stickerType = SchoolDB.Instance().fromAttribute(
                    StickerType.class, "COLOR", hexcode
            );

            if (stickerType == null && !request.requestMethod().equalsIgnoreCase("POST")) {
                halt(404, Session().httpError(404,
                        "Not Found",
                        String.format("Sticker with color #%s not found !", hexcode)
                        )
                );
            }
        } else {
            halt(401, Session().httpError(401,
                    "Unauthorized",
                    "You need to be login to modify sticker types"
                    )
            );
        }
    };

    @Override
    public Filter before() {
        return _before;
    }

    public StickerTypeListener() {
        setRoute(POST, post);
        setRoute(PUT, put);
        setRoute(DELETE, delete);
    }
}
