package io.github.madeshiro.request.listeners.post;

import io.github.madeshiro.components.Student;
import io.github.madeshiro.jdbc.SchoolDB;
import io.github.madeshiro.request.annotation.RequestRoute;
import spark.Request;
import spark.Response;
import spark.Route;

import static io.github.madeshiro.main.ServerMain.Session;
import static io.github.madeshiro.request.Method.POST;

@RequestRoute(method = POST, uri = "/students")
public class Students implements Route {
    @Override
    public Object handle(Request request, Response response) {
        if (Session().isLogin()) {
            String first = request.queryParams("firstname");
            String last  = request.queryParams("lastname");
            if (first != null && last != null) {
                Student student = SchoolDB.Instance().createComponent(Student.class,
                        new Student(first, last)
                );
                if (student != null) {
                    return Session().httpResponse(201, "Student created !",
                            String.format("The student \"%s %s\" has been created successfully !", first, last),
                            "/students"
                    );
                } else {
                    return Session().httpError(500, "Internal Server Error",
                            "An error occurred while creating a new student..."
                    );
                }
            } else {
                return Session().httpError(400, "Invalid Request",
                        "Missing argument to create a new student (firstname + lastname) !"
                );
            }
        } else {
            return Session().httpError(401, "Unauthorized",
                    "You need to be connected to perform this action"
            );
        }
    }
}
