package io.github.madeshiro.request.listeners;

import io.github.madeshiro.components.Sticker;
import io.github.madeshiro.components.StickerType;
import io.github.madeshiro.components.Student;
import io.github.madeshiro.jdbc.SchoolDB;
import io.github.madeshiro.main.Session;
import io.github.madeshiro.request.RequestListener;
import io.github.madeshiro.request.annotation.RequestRoutes;
import io.github.madeshiro.request.filters.Beforeable;
import spark.Filter;
import spark.Route;

import java.sql.Date;

import static io.github.madeshiro.main.ServerMain.Session;
import static io.github.madeshiro.request.Method.*;
import static spark.Spark.halt;

@RequestRoutes(methods = {PUT, DELETE}, uri = "/students/:name/stickers/:sticker")
public class StickersOfStudentListener extends RequestListener implements Beforeable {

    private int studentId, stickerId;
    private Student student;
    private Sticker sticker;

    private final Filter _before = (request, response) -> {
        try {
            studentId = Integer.parseInt(request.params(":name"));
            stickerId = Integer.parseInt(request.params(":sticker"));
            student = SchoolDB.Instance().fromId(Student.class, studentId);
            sticker = SchoolDB.Instance().fromId(Sticker.class, stickerId);

            if (student != null) {
                if (sticker != null) {
                    if (!sticker.getStudent().equals(student)) {
                        halt(400,
                                Session().httpError(400,
                                        "Bad Request",
                                        String.format("Sticker with id = %d isn't assign " +
                                                "to the student with id = %d",
                                                stickerId, studentId
                                        )
                                )
                        );
                    }
                } else {
                    halt(404,
                            Session().httpError(404,
                                    "Not Found",
                                    String.format("Sticker with id = %d not found !", stickerId)
                            )
                    );
                }
            } else {
                halt(404,
                        Session().httpError(404,
                                "Not Found",
                                String.format("Student with id = %d not found !", studentId)
                        )
                );
            }

        } catch (NumberFormatException e) {
            halt(400,
                    Session().httpError(400,
                            "Bad Request",
                            "Wrong student or sticker ID format (expected number)")
            );
        }
    };

    /**
     * Update a sticker
     * query params are type, date, reason
     */
    public final Route put = (request, response) -> {
        if (Session().isLogin()) {
            String queryType = request.queryParams("type");
            String queryDate = request.queryParams("date");
            String queryReason = request.queryParams("reason");

            if (queryType != null) {
                StickerType type = SchoolDB.Instance().fromAttribute(StickerType.class, "ID", Integer.parseInt(queryType));
                if (type != null) {
                    sticker.setStickerType(type);
                } else {
                    return Session().httpError(400, "Bad Request",
                            "Invalid argument : sticker type not found");
                }
            }

            if (queryDate != null) {
                sticker.setDate(Date.valueOf(queryDate));
            }

            if (queryReason != null) {
                sticker.setReason(queryReason);
            }

            if (sticker.needUpdate()) {
                if (sticker.updateSql()) {
                    return Session().httpResponse(202, "Sticker updated !",
                            "The sticker has been updated successfully !",
                            String.format("/student/%d/stickers/%d", student.getId(), sticker.getId())
                    );
                } else {
                    return Session().httpError(500, "Internal Server Error",
                            "An error occurred while updating student's sticker information..."
                    );
                }
            } else {
                return Session().httpError(400, "Bad Request",
                        "Missing argument(s) or invalid argument");
            }
        } else {
            return Session().httpError(401,
                    "Unauthorized",
                    "You need to be logged in to update a sticker");
        }
    };

    public final Route delete = (request, response) -> {
        Session session = Session();
        if (session.isLogin()) {
            SchoolDB.Instance().deleteComponent(sticker);
            response.status(200);
            response.type("text/plain");
            return String.format("Successfully delete sticker (id = %d).", stickerId);
        } else {
            return session.httpError(401,
                    "Unauthorized",
                    "You need to be logged in to delete a sticker");
        }
    };

    public StickersOfStudentListener() {
        setRoute(PUT, put);
        setRoute(DELETE, delete);
    }

    @Override
    public Filter before() {
        return _before;
    }
}
