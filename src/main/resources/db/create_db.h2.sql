create schema L2_WEBSERVER;

CREATE TABLE "L2_WEBSERVER".PROFESSOR (
    id          int             not null    unique  auto_increment,
    first_name  varchar(50)     not null,
    last_name   varchar(50)     not null,
    username    varchar(50)     not null    unique,
    passwd      varchar(255)    not null,

    PRIMARY KEY (id)
);

CREATE TABLE "L2_WEBSERVER".SESSION (
    id          int             not null    unique auto_increment,
    ip_address  varchar(50)     not null,
    user_agent  text            not null,
    user_id     int             not null,
    sid         uuid            not null    unique,
    timeout     bigint          not null,

    PRIMARY KEY (sid),
    CONSTRAINT user_id_fk
        FOREIGN KEY (user_id) REFERENCES PROFESSOR(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE "L2_WEBSERVER".STUDENT (
    id          int             not null    unique  auto_increment,
    first_name  varchar(50)     not null,
    last_name   varchar(50)     not null,

    PRIMARY KEY (id)
);

CREATE TABLE "L2_WEBSERVER".STICKER_TYPE (
    id          int             not null    unique  auto_increment,
    color       varchar(6)      not null    unique,
    description text,

    PRIMARY KEY (id)
);

CREATE TABLE "L2_WEBSERVER".STICKER (
    id          int             not null    unique  auto_increment,
    type_id     int             not null,
    assign      date            not null,
    reason      text,
    student     int             not null,
    professor   int             not null,

    PRIMARY KEY (id),
    CONSTRAINT type_id_fk
        FOREIGN KEY (type_id) REFERENCES STICKER_TYPE(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT student_id_fk
        FOREIGN KEY (student) REFERENCES STUDENT(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT professor_id_fk
        FOREIGN KEY (professor) REFERENCES PROFESSOR(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

INSERT INTO "L2_WEBSERVER".PROFESSOR values (default, 'Jeanne', 'Myrtille', 'jemyrtille', 'bestprof');
INSERT INTO "L2_WEBSERVER".PROFESSOR values (default, 'Claude', 'Framboisier', 'clframboisier', 'unhommage');

INSERT INTO "L2_WEBSERVER".STUDENT values (default, 'Pauline', 'Ezie');
INSERT INTO "L2_WEBSERVER".STUDENT values (default, 'Elie', 'Coptère');
INSERT INTO "L2_WEBSERVER".STUDENT values (default, 'Paul', 'Hitique');

INSERT INTO "L2_WEBSERVER".STICKER_TYPE values (default, '32a852', 'bonne action effectuée');
INSERT INTO "L2_WEBSERVER".STICKER_TYPE values (default, 'ffffff', 'bon investissement en classe');
INSERT INTO "L2_WEBSERVER".STICKER_TYPE values (default, 'cf2323', 'mauvaise conduite');

INSERT INTO "L2_WEBSERVER".STICKER values (default, 3, '2022-05-16', 'On n''entend que lui ces derniers temps !', 3, 2);
