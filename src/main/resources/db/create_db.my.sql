create database L2_WebServer;
use L2_WebServer;

CREATE TABLE PROFESSOR (
    ID          int             not null    unique  auto_increment,
    FIRST_NAME  varchar(50)     not null,
    LAST_NAME   varchar(50)     not null,
    USERNAME    varchar(50)     not null    unique,
    PASSWD      varchar(255)    not null,

    PRIMARY KEY (ID)
);

CREATE TABLE SESSION (
    ID          int             not null    unique auto_increment,
    IP_ADDRESS  varchar(50)     not null,
    USER_AGENT  text            not null,
    USER_ID     int             not null,
    SID         varchar(36)     not null    unique,
    TIMEOUT     bigint          not null,

    PRIMARY KEY (SID),
    FOREIGN KEY (USER_ID) REFERENCES PROFESSOR(ID)
);

CREATE TABLE STUDENT (
    ID          int             not null    unique  auto_increment,
    FIRST_NAME  varchar(50)     not null,
    LAST_NAME   varchar(50)     not null,

    PRIMARY KEY (ID)
);

CREATE TABLE STICKER_TYPE (
    ID          int             not null    unique  auto_increment,
    COLOR       varchar(6)      not null    unique,
    DESCRIPTION text,

    PRIMARY KEY (ID)
);

CREATE TABLE STICKER (
    ID          int             not null    unique  auto_increment,
    TYPE_ID     int             not null,
    ASSIGN      date            not null,
    REASON      text,
    STUDENT     int             not null,
    PROFESSOR   int             not null,

    PRIMARY KEY (ID),
    FOREIGN KEY (TYPE_ID)   REFERENCES STICKER_TYPE(ID),
    FOREIGN KEY (STUDENT)   REFERENCES STUDENT(ID),
    FOREIGN KEY (PROFESSOR) REFERENCES PROFESSOR(ID)
);

INSERT INTO PROFESSOR values (default, 'Jeanne', 'Myrtille', 'jemyrtille', 'bestprof');
INSERT INTO PROFESSOR values (default, 'Claude', 'Framboisier', 'clframboisier', 'unhommage');

INSERT INTO STUDENT values (default, 'Pauline', 'Ezie');
INSERT INTO STUDENT values (default, 'Elie', 'Coptère');
INSERT INTO STUDENT values (default, 'Paul', 'Hitique');

INSERT INTO STICKER_TYPE values (default, '32a852', 'bonne action effectuée');
INSERT INTO STICKER_TYPE values (default, 'ffffff', 'bon investissement en classe');
INSERT INTO STICKER_TYPE values (default, 'cf2323', 'mauvaise conduite');

INSERT INTO STICKER values (default, 3, DATE('2022-05-16'), 'On n\'entend que lui ces derniers temps !', 3, 2);
