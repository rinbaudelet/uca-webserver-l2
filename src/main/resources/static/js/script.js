/* script for each pages */

/**
 *
 * @param {HTMLInputElement} elem
 */
function simulateCheckbox(elem)
{
    elem.classList.toggle('button-checkbox-equiv');
}

/**
 *
 * @param form {HTMLFormElement}
 */
function removeErrorClasses(form) {
    for (let htmlElem of form.elements) {
        htmlElem.classList.remove('form-control-error');
    }
}

function rewritePage(html) {
    document.open();
    document.write(html);
    document.close();
}

/**
 *
 * @param {HTMLFormElement} form
 */
function buildQuery(form) {
    let query = '';
    let controlIndex = 0;
    for (let elem of form.elements) {
        if (elem.value != null || elem.value !== '') {
            if (elem.type === 'submit')
                break;
            else if (elem.type !== 'color')
                query += `${elem.name}=${elem.value}`;
            else
                query += `${elem.name}=${elem.value.substring(1)}`;

            if (controlIndex + 2 < form.elements.length) {
                query += '&';
            }

            controlIndex++;
        } else {
            elem.classList.toggle('form-control-error')
        }
    }

    return query;
}

/**
 *
 * @param {HTMLFormElement} form
 * @param {string} action
 * @param {function} callback
 */
function addComponent(form, action, callback = null) {
    let query = buildQuery(form);
    if (query != null) {
        let ajax = new XMLHttpRequest();
        ajax.open('POST', `${action}?${query}`, true);
        ajax.onload = function () {
            if (ajax.status >= 400) {
                if (callback === null) {
                    rewritePage(ajax.responseText);
                    return;
                }
            }

            if (callback !== null) {
                callback()
            }
            location.reload()
        }
        ajax.send(null);
    }

    return false;
}

/**
 *
 * @param {HTMLFormElement} form
 * @param {number|string} elem_id
 * @param {string} action
 * @param {function|null} callback
 */
function updateComponent(form, elem_id, action, callback = null) {
    let query = buildQuery(form);
    if (query != null) {
        let ajax = new XMLHttpRequest();
        ajax.open("PUT", `${action}/${elem_id}?${query}`);
        ajax.onload = function () {
            if (ajax.status >= 400) {
                if (callback === null) {
                    rewritePage(ajax.responseText);
                    return;
                }
            }

            if (callback !== null)
                callback(ajax);
            location.reload();
        }
        ajax.send(null);
    }

    return false;
}

function removeComponent(elem_id, action, callback = null) {
    let ajax = new XMLHttpRequest();
    ajax.open("DELETE", `${action}/${elem_id}`);
    ajax.onload = function() {
        if (ajax.status >= 400) {
            if (callback === null) {
                rewritePage(ajax.responseText);
                return;
            }
        }

        if (callback !== null)
            callback(ajax);
        location.reload()
    }
    ajax.send(null);
    return false;
}

function loadCollapseElements() {
    let collElems = document.getElementsByClassName("collapse-block");
    for (let elem of collElems) {
        elem = elem.getElementsByClassName("collapse-header")[0];
        elem.onclick = function(ev) {
            if (!(ev.target instanceof HTMLInputElement)) {
                this.classList.toggle("collapse-header-active");
                let collContent = this.nextElementSibling;
                let headerArrow = this.getElementsByClassName('collapse-arrow')[0];
                if (collContent.style.maxHeight) {
                    collContent.style.maxHeight = null;
                    if (headerArrow) {
                        headerArrow.innerHTML = '&#x25b6';
                    }
                } else {
                    collContent.style.maxHeight = collContent.scrollHeight + "px";
                    if (headerArrow) {
                        headerArrow.innerHTML = '&#x25bc';
                    }
                }
            }
        };
    }
}


loadCollapseElements();