<#-- @ftlvariable name="user" type="io.github.madeshiro.components.Professor" -->
<#-- @ftlvariable name="is_login" type="boolean" -->
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>${title}</title>
        <link rel="stylesheet" href="/stylesheets/style.css">
    </head>
    <body>
        <header>
            <a href="/stickers">Sticker type list</a>
            <a href="/students">Students list</a>
            <#if !is_login>
                <a href="/login">Log in</a>
            <#else>
                <div><a>Log as ${user.name}</a><a href="/logout">Log out</a></div>
            </#if>
        </header>
        <div id="main-content">
            <!--
            <label for="colorpicker">Color Picker:
                <input type="color" id="colorpicker" value="#0000ff">
            </label>
            <div class="collapse-block">
                <div class="collapse-header">
                    <button>Another button</button>
                </div>
                <div class="collapse-content" style="width: 100%">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>
            </div>
            -->
            <#include "${body}">
        </div>
        <footer>
            <a href="/pride.html">Rin Baudelet &copy; 2022</a>
        </footer>
        <script src="/js/script.js"></script>
    </body>
</html>