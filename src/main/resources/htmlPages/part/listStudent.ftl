<#-- @ftlvariable name="students" type="java.util.List<io.github.madeshiro.components.Student>" -->
<#-- @ftlvariable name="is_login" type="boolean" -->
<#list students as student>
    <#assign stickers = student.stickers>
    <div class="collapse-block">
        <div class="collapse-header display-component-block" id="student-${student.id}">
            <#if is_login == true>
                <input type="button" value="&#x2717" onclick="removeComponent('${student.id}', '/students')">
                <input type="button" class="toggle-form" value="&#x270e" onclick="simulateCheckbox(this);">
            </#if>
            <div class="component-values">
                <#if is_login == true>
                    <a class="component-id">#${student.id}@student</a>
                </#if>
                <a class="component-value component-flex-fit">${student.name}</a>
            </div>
            <#if is_login == true>
                <form class="component-edit" onsubmit="return updateComponent(this, ${student.id}, '/students');">
                    <label>
                        <input type="text" placeholder="first name" name="first" value="${student.firstname}">
                    </label>
                    <label>
                        <input type="text" placeholder="last name" name="last" value="${student.lastname}">
                    </label>
                    <input type="submit" class="component-manage-button" value="&#x21aa">
                </form>
            </#if>
            <a class="component-link" href="/students/${student.id}">&#x26F6</a>
            <a class="collapse-arrow">&#x25b6</a>
        </div>
        <div class="collapse-content">
            <#include "listSticker.ftl">
        </div>
    </div>
</#list>
<#if is_login == true>
    <form class="form-add-component" onsubmit="return addComponent(this, '/students')">
        <label class="component-flex-fit">
            <input type="text" name="firstname" placeholder="first name">
        </label>
        <label class="component-flex-fit">
            <input type="text" name="lastname" placeholder="last name">
        </label>
        <input type="submit" value="Create student">
    </form>
</#if>
