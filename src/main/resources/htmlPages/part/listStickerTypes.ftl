<#-- @ftlvariable name="is_login" type="boolean" -->
<#-- @ftlvariable name="stickers" type="java.util.List<io.github.madeshiro.components.StickerType>" -->
<#list stickers as type>
    <div class="display-component-block" id="type-${type.id}">
        <#if is_login == true>
            <input type="button" value="&#x2717" onclick="removeComponent('${type.color}', '/stickers')">
            <input type="button" class="toggle-form" value="&#x270e" onclick="simulateCheckbox(this);">
        </#if>
        <div class="component-values">
            <a class="sticker-color" style="--uca-sticker-color: #${type.color}"></a>
            <#if is_login == true>
                <a class="component-id">#${type.id}@type</a>
            </#if>
            <a class="component-value component-flex-fit">${type.description}</a>
        </div>
        <#if is_login == true>
            <form class="component-edit" onsubmit="return updateComponent(this, '${type.color}', '/stickers');">
                <label>
                    <input type="color" name="color" value="#${type.color}">
                </label>
                <label class="component-flex-fit">
                    <input type="text" name="desc" placeholder="description..." value="${type.description}">
                </label>
                <input type="submit">
            </form>
        </#if>
    </div>
</#list>
<#if is_login == true>
<form class="form-add-component" onsubmit="return addComponent(this, '/stickers/' + this['color'].value.substring(1))">
    <label>
        <input type="color" name="color" value="#ffffff">
    </label>
    <label class="component-flex-fit">
        <input type="text" name="desc" placeholder="description...">
    </label>
    <input type="submit" value="Create type">
</form>
</#if>