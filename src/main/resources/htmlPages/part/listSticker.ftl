<#-- @ftlvariable name="sticker_types" type="java.util.List<io.github.madeshiro.components.StickerType>" -->
<#-- @ftlvariable name="stickers" type="java.util.List<io.github.madeshiro.components.Sticker>" -->
<#-- @ftlvariable name="is_login" type="boolean" -->
<#list stickers as sticker>
    <div class="display-component-block" id="sticker-${sticker.id}">
        <#if is_login == true>
            <input type="button" value="&#x2717" onclick="removeComponent('${sticker.id}', '/students/${student.id}/stickers')">
            <input type="button" class="toggle-form" value="&#x270e" onclick="simulateCheckbox(this);">
        </#if>
        <div class="component-values">
            <a class="sticker-color" style="--uca-sticker-color: #${sticker.type.color}"></a>
            <#if is_login == true>
                <a class="component-id">#${sticker.id}@sticker</a>
            </#if>
            <a class="component-value component-flex-fit">${sticker.reason}</a>
            <a class="component-value">From: ${sticker.author.name}</a>
            <a class="component-value">${sticker.date}</a>
        </div>
        <#if is_login == true>
            <form class="component-edit" onsubmit="return updateComponent(this, ${sticker.id}, '/students/${student.id}/stickers');">
                <label class="component-edit-selector">
                    <select name="type">
                        <#list sticker_types as type>
                            <#if type.id == sticker.type.id>
                                <option selected value="${type.id}">${type.description}</option>
                            <#else>
                                <option value="${type.id}">${type.description}</option>
                            </#if>
                        </#list>
                    </select>
                </label>
                <label class="component-flex-fit">
                    <input type="text" placeholder="reason..." name="reason" value="${sticker.reason}">
                </label>
                <input type="submit" class="component-manage-button" value="&#x21aa">
            </form>
        </#if>
    </div>
</#list>
<#if is_login>
<form class="form-add-component" onsubmit="return addComponent(this, '/students/${student.id}/stickers')">
    <label>
        <select name="color">
            <#list sticker_types as type>
                <option value="${type.color}">${type.description}</option>
            </#list>
        </select>
    </label>
    <label class="component-flex-fit">
        <input type="text" name="reason" placeholder="reason">
    </label>
    <label>
        <input type="date" name="date" value="2022-05-23">
    </label>
    <input type="submit" value="create sticker">
</form>
</#if>