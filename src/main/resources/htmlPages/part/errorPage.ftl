<div id="error-page">
    <h1>${error_code} - ${error_title}</h1>
    <p>
        ${error_description}
    </p>
    <img height="253" width="212" src="/images/notfound.gif" alt="Henry Stickman dancing">
</div>