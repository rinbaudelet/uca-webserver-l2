<#-- @ftlvariable name="redirection_link" type="java.lang.String" -->
<#-- @ftlvariable name="done_title" type="java.lang.String" -->
<#-- @ftlvariable name="done_description" type="java.lang.String" -->
<div id="done-page">
    <h1>${done_title}</h1>
    <p>
        ${done_description}
    </p>
    <a href="${redirection_link}">Redirection</a>
</div>